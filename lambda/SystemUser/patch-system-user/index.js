exports.handler = async (event, context, callback) => {
  try {
    context.callbackWaitsForEmptyEventLoop = false;

    console.log('GET patch system-user called');
    return callback(null, {});
  } catch (err) {
    console.log('Error patch system-user:', err);
    return callback(null, err.message);
  }
};
