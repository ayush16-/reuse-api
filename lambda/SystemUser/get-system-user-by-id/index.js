exports.handler = async (event, context, callback) => {
  try {
    context.callbackWaitsForEmptyEventLoop = false;

    console.log('GET get system-user by id called');
    return callback(null, {});
  } catch (err) {
    console.log('Error get system-user by id:', err);
    return callback(null, err.message);
  }
};
