exports.handler = async (event, context, callback) => {
  try {
    context.callbackWaitsForEmptyEventLoop = false;

    console.log('GET post system-user called');
    return callback(null, {});
  } catch (err) {
    console.log('Error post system-user:', err);
    return callback(null, err.message);
  }
};
